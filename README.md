# conan-RocksDB


[![Conan](https://api.bintray.com/packages/gocarlos/public-conan/rocksdb:gocarlos/images/download.svg) ](https://bintray.com/gocarlos/public-conan/rocksdb:gocarlos/_latestVersion)
[![pipeline status](https://gitlab.com/gocarlos/conan-rocksdb/badges/master/pipeline.svg)](https://gitlab.com/gocarlos/conan-rocksdb/commits/master)

## lib producer

```bash
# build conan package
conan create . gocarlos/testing --build missing
# upload package to gocarlos artifactory
conan upload rocksdb -r gocarlos
```

## lib consumer

```bash
conan remote add gocarlos https://api.bintray.com/conan/gocarlos/public-conan

cat << EOF >> conanfiile.txt
[requires]
rocksdb/6.4.6@gocarlos/testing

[generators]
cmake_find_package
cmake_paths

[options]
EOF
```
