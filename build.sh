#!/bin/bash

rm -rf build
mkdir build
cd build
cmake ../test_package
cmake --build . -j
