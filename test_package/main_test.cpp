
#include <cstdlib>
#include <iostream>

#include "rocksdb/db.h"

int main() {
  rocksdb::DB *db{nullptr};
  rocksdb::Options options;
  options.create_if_missing = true;
  rocksdb::Status status = rocksdb::DB::Open(options, "/tmp/systemx_systemx_db", &db);
  std::cout << std::boolalpha << "status ok: " << status.ok()
            << ", mesage: " << status.ToString() << std::endl;
  status = db->Put(rocksdb::WriteOptions(), "response", "42");
  std::cout << std::boolalpha << "status ok: " << status.ok()
            << ", mesage: " << status.ToString() << std::endl;
  std::string value;
  status = db->Get(rocksdb::ReadOptions(), "response", &value);
  std::cout << "value: " << value << std::endl;
  std::cout << std::boolalpha << "status ok: " << status.ok()
            << ", mesage: " << status.ToString() << std::endl;

  return EXIT_SUCCESS;
}
